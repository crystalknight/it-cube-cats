import 'package:flutter/material.dart';

class PawWidget extends StatelessWidget {
  const PawWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 24,
      height: 24,
      child: Image.asset('./assets/paw.png'),
    );
  }

}