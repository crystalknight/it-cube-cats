import 'level_entity.dart';

class GameController {

  final int _area = 25;
  final List<LevelEntity> _levels = [];
  int _currentLevel = 0;

  GameController() {
    _levels.add(LevelEntity('./assets/level1.jpeg', 0.48, 0.283));
  }

  LevelEntity _getCurrentLevel() {
    return _levels[_currentLevel];
  }

  String getLevelImage() {
    return _getCurrentLevel().levelImage;
  }

  bool catTouched(double xPoint, double yPoint, double screenWidth, double screenHeight) {
    LevelEntity currentLevel = _getCurrentLevel();
    double catXPosition = screenWidth * currentLevel.xCoefficient;
    double catYPosition = screenHeight * currentLevel.yCoefficient;

    bool xCondition = (xPoint - catXPosition).abs() <= _area;
    bool yCondition = (yPoint - catYPosition).abs() <= _area;

    return xCondition && yCondition;
  }

  bool nextLevel() {
    if (_currentLevel + 1 < _levels.length) {
      _currentLevel++;
      return true;
    } else {
      return false;
    }
  }

}