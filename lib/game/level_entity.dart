class LevelEntity {

  final String _levelImage;
  final double _xCoefficient;
  final double _yCoefficient;

  LevelEntity(this._levelImage, this._xCoefficient, this._yCoefficient);

  double get yCoefficient => _yCoefficient;

  double get xCoefficient => _xCoefficient;

  String get levelImage => _levelImage;
}