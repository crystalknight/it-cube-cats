import 'package:flutter/material.dart';
import 'package:it_cube/game/game_controller.dart';
import 'package:it_cube/paw_widget.dart';

class GameScreen extends StatefulWidget {
  const GameScreen({super.key});

  @override
  State<StatefulWidget> createState() {
    return _GameScreenState();
  }
}

class _GameScreenState extends State<GameScreen> {
  final GameController gameController = GameController();

  Positioned? paw;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GestureDetector(
        onTapDown: (TapDownDetails tapDetails) async {
          Offset position = tapDetails.localPosition;
          double x = position.dx;
          double y = position.dy;

          Size size = MediaQuery.of(context).size;
          double width = size.width;
          double height = size.height;

          setState(() {
            paw = Positioned(left: x - 12, top: y - 12, child: const PawWidget());
            removePaw();
          });

          if (gameController.catTouched(x, y, width, height)) {
            if (!gameController.nextLevel()) {
              await showEndDialog();
            }
          }
        },
        child: Stack(
          children: [
            Image(
              height: double.infinity,
              width: double.infinity,
              fit: BoxFit.fill,
              image: AssetImage(gameController.getLevelImage()),
            ),
            pawWidget(),
          ],
        ),
      ),
    );
  }

  Widget pawWidget() {
    return paw == null ? Container() : paw!;
  }

  removePaw() async {
    Future.delayed(const Duration(milliseconds: 350), (){
      setState(() {
        paw = null;
      });
    });
  }

  showEndDialog() async {
    await showDialog(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: const Text('Все носы найдены!'),
          content: const Text('Вы великолепны!'),
          actions: [
            TextButton(
              child: const Text('Закрыть'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
    Navigator.pop(context);
  }
}
