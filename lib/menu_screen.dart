import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:it_cube/game_screen.dart';

class MenuScreen extends StatefulWidget {
  const MenuScreen({super.key});

  @override
  State<StatefulWidget> createState() {
    return _MenuScreenState();
  }
}

class _MenuScreenState extends State<MenuScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          SizedBox(
              height: 200, child: Image.asset('./assets/it_cube_logo.png')),
          SizedBox(
            height: 200,
            child: SvgPicture.asset('./assets/ediweb_logo.svg',
                semanticsLabel: 'Ediweb Logo'),
          ),
          ElevatedButton(
              onPressed: () {
                Navigator.push(context, MaterialPageRoute(builder: (context) {
                  return const GameScreen();
                }));
              },
              child: const Text('Запустить'))
        ],
      ),
    );
  }
}
